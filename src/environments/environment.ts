// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDEboVYlpElzKGjqXlRewIVP1kWPYbezmU",
    authDomain: "todolistapp-25277.firebaseapp.com",
    databaseURL: "https://todolistapp-25277.firebaseio.com",
    projectId: "todolistapp-25277",
    storageBucket: "todolistapp-25277.appspot.com",
    messagingSenderId: "501703423603"
  }

};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
